package com.greatLearning.assignment;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public class Employee extends DataStructureA {
	
	int id;
	String name;
	int age;
	int salary;
	String department;
	String city;
	public void setId(int n) {
		int id = n;
		}
	
	public void age(int k) {
		int age = k;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getSalary() {
		return salary;
	}
	public void setSalary(int salary) {
		this.salary = salary;
	}
	public String getDepartment() {
		return department;
	}
	public void setDepartment(String department) {
		this.department = department;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	
    public static void main(String[] args) {
		// TODO Auto-generated method stub
   Employee e = new Employee();
   
   System.out.print("S.No. " + "Name " +"  Age  "+ "Salary(INR) " + "Department " + "Location");
   System.out.println();
   System.out.println();
   System.out.print("1  "+"   Aman "+"  20  "+" 1,100,000" +"    IT "+"        Delhi");
   System.out.println();
   System.out.print("2  "+"   Bobby "+" 22  "+" 500,000" +"      HR "+"        Bombay");
   System.out.println();
   System.out.print("3  "+"   Zoe "+"   20  "+" 750,000" +"      Admin "+"     Delhi");
   System.out.println();
   System.out.print("4  "+"   Smitha "+"21  "+" 1,000,000" +"    IT "+"        Chennai");
   System.out.println();
   System.out.print("5  "+"   Smitha "+"24  "+" 1,200,000" +"    HR "+"        Bengaluru");
   System.out.println();
   System.out.println();
    	
    	e.setId(1);
    	e.setId(2);
    	e.setId(3);
    	e.setId(4);
    	e.setId(5);
    	
    	e.age(20);
    	e.age(22);
    	e.age(20);
    	e.age(21);
    	e.age(24);
    	
		
		ArrayList<String> employees = new ArrayList<String>();
		employees.add("Aman");
		employees.add("Bobby");
		employees.add("Zoe");
		employees.add("Smitha");
		employees.add("Smitha");
		
		
				
				e.sortingNames(employees);
				System.out.println("Names of all employees in the sorted order are:");
				System.out.println(employees);
				System.out.println();
		
				ArrayList<String> employee = new ArrayList<String>();
				employee.add("Delhi");
				employee.add("Bombay");
				employee.add("Delhi");
				employee.add("Chennai");
				employee.add("Bengaluru");
				System.out.println("Count of Employees from each city:");
				System.out.print("{");
				e.cityNameCount(employee);
			
				System.out.println("}");
				System.out.println();
				
				ArrayList<Integer> employeee = new ArrayList<Integer>(6);
				employeee.add(1100000);
				employeee.add(500000);
				employeee.add(750000);
				employeee.add(1000000);
				employeee.add(1200000);
				System.out.println("Monthly Salary of employee along with their ID is:");
				System.out.print("{");
				e.monthlySalary(employeee);
				System.out.println("}");
				

	}
	
}
